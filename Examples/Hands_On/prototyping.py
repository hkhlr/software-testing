# SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <tim.jammer@hpc-hessen.de>
#
# SPDX-License-Identifier: MIT

from power_method import power_method_iterations

import numpy as np

RTOL = 1e-2

def main() -> None:
    factor = 2

    A = np.loadtxt("./test_data/matrix_symmetric/mat.50.3.txt")

    A= np.asarray([[1.00000e+00, 1.11834e+05],
           [1.00000e+00, 1.00000e+00]])
    eigenvalue, eigenvector, _ = power_method_iterations(A, maxit=1000, criterion="eigenvectors")
    
    eigenvector_scaled = factor * eigenvector
    
    dotp_scaled = np.linalg.norm(np.dot(A, eigenvector_scaled)) ** 2
    
    print(dotp_scaled / np.linalg.norm(eigenvector_scaled) ** 2 , eigenvalue ** 2)
    print(np.allclose(np.dot(A, eigenvector_scaled) / eigenvalue, eigenvector_scaled,rtol=RTOL, atol=RTOL * 1e-3))

if __name__ == "__main__":
    main()

