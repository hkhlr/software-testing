# SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <tim.jammer@hpc-hessen.de>
#
# SPDX-License-Identifier: MIT

import numpy as np

import pytest
from hypothesis import given, settings, strategies as st
import hypothesis.extra.numpy

import sys
import importlib
from pathlib import Path
from glob import glob

from power_method import power_method_iterations

MAX_ITERATIONS = 1000
MAX_EXAMPLES = 100
RTOL = 1e-2


@pytest.mark.parametrize(
    'matrix_file',
    glob('./test_data/matrix_symmetric/mat*.txt')
)
def test_real_symmetric_iter_eigenvalues(matrix_file):
    A = np.loadtxt(matrix_file)

    eval_max_iter, _, _ = power_method_iterations(
        A, maxit=MAX_ITERATIONS, criterion="eigenvalues")
    evals = np.linalg.eigvalsh(A)  # use different implementation to compute eigenvalues
    eval_max = evals[np.argmax(np.abs(evals))]
    # The choice of atol keeps the same ratio as the default values:
    # https://docs.scipy.org/doc/numpy/reference/generated/numpy.allclose.html
    assert np.allclose(eval_max, eval_max_iter, rtol=RTOL, atol=RTOL * 1e-3)  # do the comparision


def test_criterion():
    A = np.asarray([[1, 2], [3, 4]])
    with pytest.raises(ValueError):
        eval_max_iter, _, _ = power_method_iterations(A, maxit=MAX_ITERATIONS, criterion="iterations")
