# SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <tim.jammer@hpc-hessen.de>
#
# SPDX-License-Identifier: MIT

import pytest

def m(x):
    return 2 * x

def test_function_2():
    assert m(2) == 4

@pytest.mark.parametrize("test_input,expected", [(4, 8), (16, 32), (64, 128)])
def test_function_x(test_input, expected):
    assert m(test_input) == expected

def d(x):
    return 2 / x

def test_function_by_zero():
    # tell Pytest to expect a ZeroDivisionError:
    with pytest.raises(ZeroDivisionError):
        d(0)

def test_wrong():
    assert m(2) ==2

def test_wrong_by_zero():
    with pytest.raises(ZeroDivisionError):
        d(2)

from hypothesis import given, settings, strategies as st

@settings(max_examples=1000)
@given(st.integers(min_value=0,max_value=1000))
def test_function_even(x):
    assert m(x) % 2 == 0,  "result was odd, should be even"
