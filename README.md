<!--
SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <tim.jammer@hpc-hessen.de>, <marcel.giar@hpc-hessen.de>

SPDX-License-Identifier: CC0-1.0
-->

# Testing of Scientific Software

## About this course

This course, offered by the Competence Center for High Performance Computing in Hessen ([HKHLR](https://www.hkhlr.de/)), teaches the basics of testing scientific software. For a detailed course description please refer to the [course website](https://www.hkhlr.de/en/events/testing-scientific-software-2022-10-17).


## Getting the course material 

The course material can be obtained by cloning this repository. E.g. from within a terminal emulator of your choice execute:

```shell
git clone https://git.rwth-aachen.de/hkhlr/software-testing.git
```

Please note that you need to have Git installed. This can e.g. be done via `sudo apt-get install git` on Debian-based Linux distros or `brew install git` on macOS with [Homebrew](https://brew.sh/) installed. Git for Windows can be obtained [here](https://git-scm.com/downloads).


## Setting up the environment
Instructions to set up the environment for the exercises are contained in [Examples/Hands_On/README.md](Examples/Hands_On/README.md)